// console.log(`Hello World!`);

// function printInput() {
//   let nickname = prompt(`Enter your nickname: `);
//   console.log(`Hi, ${nickname}`);
// }

// printInput();

function printName(name = "John Lloyd") {
  console.log(`My name is ${name}`);
}
printName("EJ");

let variableName = "Edward";
printName(variableName);

function noParam() {
  let param = "No parameter";
  console.log(param);
}

noParam("With Parameter");

function checkDivisibilityBy8(num) {
  let modulo = num % 8;
  console.log(`The remainder of ${num} is ${modulo}`);

  let isDivisibleBy8 = modulo === 0;
  console.log(`Is ${num} divisible by 8?`);
  console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(675);

function argumentFunction() {
  console.log(
    `This function was passed as an argument before the message was printed`
  );
}
function argumentFunctionTwo() {
  console.log(
    `This function was passed as a second argument before the message was printed`
  );
}

function invokeFunction(argFunction) {
  argFunction();
}
invokeFunction(argumentFunction);
invokeFunction(argumentFunctionTwo);

// Multiple parameters

function createFullName(firstName, middleName, lastName) {
  console.log(`This is firstName: ${firstName}`);
  console.log(`This is middleName: ${middleName}`);
  console.log(`This is lastName: ${lastName}`);
}

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela", "Cruz", "EJ");
createFullName("Juan", "Dela");

// Return
function returnFullName(firstName, middleName, lastName) {
  console.log(`${firstName} ${middleName} ${lastName}`);
}
returnFullName("Ada", "None", "Lovelace");

function returnName(firstName, middleName, lastName) {
  return `${firstName} ${middleName} ${lastName}`;
}

console.log(returnName("John", "Doe", "Smith"));
const returnLog = returnName("John", "Doe", "Smith");
console.log(returnLog);

function printPlayerInfo(userName, level, job) {
  console.log(`Username: ${userName}`);
  console.log(`Level: ${level}`);
  console.log(`Job: ${job}`);
  return `${userName}, ${level}, ${job}`;
}
printPlayerInfo("boy_wonder", 98, "Archer");

let user1 = printPlayerInfo("boy_wonder", 98, "Archer");
console.log(user1);
